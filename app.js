var express = require('express')
  , app = express()
  , http = require('http')
  , socket = require('socket.io');
 
app.configure(function(){
	app.use(express.static(__dirname));
});

var server = app.listen(3000);
var io = socket.listen(server);

io.sockets.on('connection', function (socket) {
	console.log("se ha conectado un nuevo cliente");
	socket.on('prueba',function(){
		console.log("el socket se ha iniciado correctamente");
		io.sockets.emit('nuevo mensaje');
	});

	socket.on('mensaje', function(mensaje){
		console.log('mensaje recibido y despachado');
		socket.get('nickname', function (err, name) {
      	io.sockets.emit('nuevoMensaje', name+': '+mensaje);
      	});
		
	});

	socket.on('setNickname', function(name){
		socket.set('nickname', name, function(){
			console.log('nuevo nickname ' + name);
			socket.emit('Listo');
		});

	});


});



